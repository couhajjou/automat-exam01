# Hackers News

A programming test by Automat.

---
## setup

Clone the repo:
```sh
git clone git@bitbucket.org:couhajjou/automat-exam01.git
cd automat-exam01
npm install
```

## Run the crawler

`npm start` will do this: 

- start crawling, 
- print results on the screen,
- generate the file `results.html`.

Then just open `results.html` in your browser to see the results in html format.