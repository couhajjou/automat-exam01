"use strict"

let assert = require('assert')

// A generic Crawler for distributed json ressources ==========================
class Crawler {
    constructor() {
        this.store = {}
        this.fetchNode = function (id) {}
        this.handlers = new Map()
        this.allowedChildren = new Map()
    }
    useStorage(store) {
        this.store = store
        return this
    }
    useFetcher(fetcher) {
        this.fetchNode = fetcher
        return this
    }
    addHandler(nodeType, handler) {
        this.handlers.set(nodeType, handler.bind(this)) 
        return this
    }
    allowChildren(nodeType, allowedChildrenTypes) {
        this.allowedChildren.set(nodeType, allowedChildrenTypes)
        return this
    }
    run(nodes) {
        console.log()
        process.stdout.write('Running crawler ...')
        assert(nodes.then, `expecting a Promise`)

        return nodes.map((id, index) => {
            return this.crawlNode({id: id, parentType: "root"}, index)
        }, {concurrency: 10})
    }
    crawlNode({id, parentType}, index) {
        let self = this

        return this.fetchNode(id)
            .then( (node) => {
                process.stdout.write('.')

                // skip this node if it is not allowed
                let allowedTypes = self.allowedChildren.get(parentType)
                if(! allowedTypes.includes(node.type))
                    return []

                // do the SIDE EFFECT
                let handler = self.handlers.get(node.type)
                handler(node, index)

                // skips all kids if the allow types is empty
                let allowedChildrenTypes = self.allowedChildren.get(node.type)
                if (! allowedChildrenTypes || allowedChildrenTypes.length === 0)
                    return []

                // return children for further processing
                let childrenIds = node.kids || []
                let children = childrenIds.map( (id) => { 
                    return {id: id, parentType: node.type}  
                })
                return children

            }).map( function ({id, parentType}, index ) {
                return self.crawlNode(
                    {id: id, parentType: parentType}, 
                    index 
                )
            }, {concurrency: 100})
    }
}

module.exports = Crawler

