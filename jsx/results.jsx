import React from 'react'
import Head from './head.jsx'
import Body from './body.jsx'
import data from '../results.json'

class Root extends React.Component {
    render () {
        return (
            <html>
                <Head title='HN Top Stories | Automat' />
                <Body data={data}/>
            </html>
        )
    }
}

export default Root

