import React from 'react'

import StoriesTable from './stories-table.jsx'
import CommentatorsTable from './commentators-table.jsx'

const Body = ({ data }) => (
    <body className="ui text container">
        <h2 className="ui header" style={{padding: '1em 0 1em 0'}}>
            Hackers News
            <div className="sub header">A programming test by Automat</div>
        </h2>
        <div className="ui grid">
            <div className="eleven wide column">
                <StoriesTable stories={data.stories}></StoriesTable>
            </div>
            <div className="five wide column">
                <CommentatorsTable commentators={data.commentators}></CommentatorsTable>
            </div>
        </div>
        <div className="ui vertically divided grid container">
            <div className="two column">
            </div>
            <div className="column">
                <p></p>
                <p></p>
            </div>
        </div>
    </body>
)

export default Body

