import React from 'react'

const Head = ({ title }) => (
    <head>
        <meta charSet='utf-8' />
        <title>{title}</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.7/semantic.min.css"/>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/semantic-ui/2.2.7/semantic.min.js"></script>
    </head>
)

export default Head

