import React from 'react'

const StoriesTable = ({ stories }) => (
    <table className="ui small compact table">
        <thead>
            <tr>
                <th colSpan="2">
                    Top Stories
                </th>
            </tr>
        </thead>
        <tbody>
            {stories.map( (story, index) => {
                // weird bug: a null object is in stories
                if(! story) 
                    return 

                return <tr key={story.id}>
                    <td className="disabled top aligned">{index + 1}.</td> 
                    <td><a href={story.url}>{story.title}</a></td>
                </tr>
            })}
        </tbody>
    </table>
)

export default StoriesTable
