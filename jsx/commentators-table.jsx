import React from 'react'

const CommentatorsTable = ({ commentators }) => (
    <table className="ui small compact table">
        <thead>
            <tr><th colSpan="2">
                    Top Commentators
            </th></tr>
        </thead>
        <tbody>
            {commentators.map( ([commentator, count]) =>
                <tr key={commentator}>
                    <td>{commentator}</td> 
                    <td>
                        <div className="ui small label">
                            <i className="comment icon"></i>{count}
                        </div> 
                    </td> 
                </tr>
            )}
        </tbody>
    </table>
)

export default CommentatorsTable

