"use strict"

let process = require('process')
let assert = require('assert')
var fs = require('fs')
let rp = require('request-promise')
let Crawler = require('./crawler.js')

// Storage for results ========================================================
let store = {
    stories: [],
    commentators: new Map(),
    stats: {
        fetchCounter: 0,
        time: 0
    }
}

// Fetching data ==============================================================
function dontGiveUp(f) {
    return f().then(
        undefined, // pass through success
        function (err) {
            if (err) {
                process.stdout.write('R')
                return dontGiveUp(f) 
            }
            throw err 
        }
    )
}

function fetch(uri) {
    store.stats.fetchCounter++
    const HACKER_NEWS_API = "https://hacker-news.firebaseio.com/v0"
    // console.log(`fetch: ${HACKER_NEWS_API}/${uri}`)

    return dontGiveUp(function() {
        return rp({
            uri: `${HACKER_NEWS_API}/${uri}`,
            json: true,
            timeout: 2000
        })
    })
}

function fetchNode(id) {
    return fetch(`item/${id}.json`)
}

// Promise the roots nodes ====================================================
let storiesPromise = fetch('topstories.json')
    .then(function (stories){
        return stories.slice(0, 30)
    })

// Setup the story crawler ====================================================
let storyCrawler = new Crawler()
    .useStorage(store)
    .useFetcher(fetchNode)
    .allowChildren("root", ["story"])
    .addHandler("story",  function (node, index) {
        this.store.stories[index] = node
    })
    .allowChildren("story",  ["comment"]) 
    .addHandler("comment",  function (node, index) {
        if(! node.by || node.deleted)
            return

        let commentCount = this.store.commentators.get(node.by) || 0 
        commentCount++
        this.store.commentators.set(node.by, commentCount)
    })
    .allowChildren("comment",  ["comment"])

// Most active commentators ===================================================
function sortTopCommentators() {
    console.log("Sorting commentators ...")
    store.commentators = 
        [...store.commentators]
        .sort( (a, b) => b[1] - a[1] )
        .slice(0, 10)
}
// Show the results ===========================================================
function showResults() {

    console.log("Showing results ...")
    console.log()
    console.log("TOP COMMENTATORS:")
    console.log("=================")
    let index = 0
    store.commentators.forEach(function ([commentator, count]) {
        console.log(`${++index}: ${commentator} - comments: ${count}`)
    })

    console.log()
    console.log("TOP STORIES:")
    console.log("============")
    store.stories.forEach(function(story, index) {
        console.log(`${index + 1}: ${story.title}`)
    })
}
// Save the results in a file =================================================
function saveResults() {
    const file = "results.json"

    console.log(`Saving ${file} ...`)
    fs.writeFileSync(file, JSON.stringify(store, null, '\t'))
}

// Run the show ===============================================================
let start = new Date()

console.log()
console.log("Crawler Legend:   . = Fetch successful   R = Retring failed fetch ")

storyCrawler.run(storiesPromise)
    .then(function() {
        process.stdout.write("done")
        console.log()

        sortTopCommentators()
        saveResults()
        showResults()

        let finish = new Date()
        console.log()
        console.log("http requests: ", store.stats.fetchCounter)
        store.stats.time = (finish - start) / 1000
        console.log(`Time: ${store.stats.time}sec`)

    }).catch(function (err) {
        console.log()
        console.log("**********************************************")
        console.log("* ", err.message)
        console.log("**********************************************")
        console.log("exiting ...")
        process.exit(1)
    })

